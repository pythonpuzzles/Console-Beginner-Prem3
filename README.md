# Python Console Beginners Premium Puzzle 3
## Thank you, for contributing to the channel! 
### Your money helps support me and allows me to keep making these learning puzzles.

### View the YouTube Video: 


## Puzzles
In main.py, I have commented out some starting code for these puzzles. <br />
Write your code in these sections and uncomment them. <br />
I have provided some examples of similar functions to what each puzzle is asking for. Learn to read the code and modify it. <br />

At the very bottom, I have listed the functions. Comment and uncomment them, to run them one at a time.

```
# Run the puzzles

    example_a()
    # puzzle_a()

    example_b()
    # puzzle_b()

    example_c()
    # puzzle_c()
```

### A - More Maths
Solve these maths problems <br />


### B - AmsterDAMNN!
An American tourist is going to Amsterdam. <br />
Marijuana joints cost 30 Euros. <br />
A pint of Heineken beer costs 15 Euros. <br />
The American tourist wants to spend $500 USD "getting wasted". <br />
Write a program that calculates the maximum beers or joints they could buy <br />
Look up the USD to Euro exchange rate <br />
Remember to ROUND DOWN. If you can only afford 0.666 of a drink they won't sell it to you! <br />


### C - Art of the deal
Donald Trump wants to automate deal making! <br />
Write a program for him that asks the user to enter their best offer <br />
"Enter your best offer for this deal in USD (fantastic currency, the best!): " <br />
Then just counter-offer between 10% and 50% of that amount. <br />
Don't expect to get paid for writing this program.... <br />

Don't worry about a user entering something that may crash the program <br />
We have not done if statements or error checking yet <br />


# Good Luck!