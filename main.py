from decimal import Decimal
import random
import math


def example_a():
    print('\nExample A')
    print('~~~~~~~~~')

    print("If we have a circle with a radius of 4cm, what is it's area?")

    pi_symbol = "π"                         # Literal Character. Or could have used unicode \u03A0"

    print(f"Formula is 2 {pi_symbol} r")

    radius = 4
    pi = 3.1416
    # pi = math.pi                            # Alternative is to use Math module. "import math" at top
    area = pi * radius * radius

    print(f"Answer: {area} cm\u00B2")       # "\u00B2" is the squared symbol in unicode


# Puzzle A - More Maths
#
# Solve these maths problems
def puzzle_a():
    print('\nPuzzle A')
    print('~~~~~~~~~')

    print("A circle has a diameter of 100m. What is the circumference?")

    print("\nFind the hypotenuse (longest side) of a right-angled triangle, if the other 2 sides are 5cm long and 12cm long?")


def example_b():
    print('\nExample B')
    print('~~~~~~~~~')

    # A US dollar is worth 1.49 Dollerydoos
    # Remember to use decimal.Decimal() for money values
    exchange_rate = Decimal(1.49)

    usd_money = Decimal(100)
    aus_money = usd_money * exchange_rate

    print(f"{usd_money} USD is approximately {aus_money} AUD. [No quantize]")

    # Use quantize to force 2 decimal places with floating point numbers.
    aus_money = aus_money.quantize(Decimal("0.01"))

    print(f"{usd_money} USD is approximately {aus_money} AUD.")
    print("That's a lot of dollerydoos!")
    print("That'll get me a carton of piss (24 cans of beer) for sure bro!")


# Puzzle B - AmsterDAMNN!
#
# An American tourist is going to Amsterdam.
# Marijuana joints cost 30 Euros.
# A pint of Heineken beer costs 15 Euros.
# The American tourist wants to spend $500 USD "getting wasted".
# Write a program that calculates the maximum beers or joints they could buy
# Look up the USD to Euro exchange rate
# Remember to ROUND DOWN. If you can only afford 0.666 of a drink they won't sell it to you!
def puzzle_b():
    print('\nPuzzle B')
    print('~~~~~~~~~')


def example_c():
    print('\nExample C')
    print('~~~~~~~~~~~')

    print("How many Donald Trumps does it take to screw in a lightbulb?")

    donalds = random.randint(1, 100)

    print(f"Maybe {donalds} ?")

    print("No. Just one, but he’ll claim it was the best lightbulb change in history!")


# Puzzle C - Art of the deal
#
# Donald Trump wants to automate deal making!
# Write a program for him that asks the user to enter their best offer
# "Enter your best offer for this deal in USD (fantastic currency, the best!): "
# Then just counter-offer between 10% and 50% of that amount.
# Don't expect to get paid for writing this program....

# Don't worry about a user entering something that may crash the program
# We have not done if statements or error checking yet
def puzzle_c():
    print('\nPuzzle C')
    print('~~~~~~~~~~~')


if __name__ == '__main__':

    # Run the puzzles

    example_a()
    # puzzle_a()

    example_b()
    # puzzle_b()

    example_c()
    # puzzle_c()
